# Assignment 5
Group Name: Sokamerniki

## Members

- 21B030723 Toleugaliyev Nurdaulet
- 21B030915 Sarshaev Erkhan
- 21B030662 Glazhdin Sabir
- 21B030903 Rymkul Yerassyl
- 21B031212 Bauyrzhan Kilybai
- 21B030667 Zhagypar Gabdylgaziz


## Installation and Execute

Use the Docker to launch the project.

```bash
docker build -t front ./fruits-kz
docker build -t backend ./fruits-kz-back
docker build -t etl ./etl-job
docker compose up
```

## To check logs

```bash
docker ps #to check running docker containers
docker logs <id-of-container>
```

