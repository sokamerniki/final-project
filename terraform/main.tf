terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.51.0"
    }
  }
}

provider "google" {
  credentials = file("terraform-devops-406210-7a94b783a432.json")

  project = "terraform-devops-406210"
  region  = "asia-east2"
  zone    = "asia-east2-b"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}

resource "google_compute_instance" "example_instance" {
  name         = "example-instance"
  machine_type = "n1-standard-1"
  zone         = "asia-east2-b"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
    }
  }

  network_interface {
    network = google_compute_network.vpc_network.name
    access_config {
    }
  }
  provisioner "remote-exec" {
    script = "/setup.sh"
  }

  metadata = {
    ssh-keys = "erhan:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDgVwgJyGYdcTso1e3MrMOX++isD96gAtcIgIWuxl+KU8gSdcifA5lfxqjzfGxAxOXeu/P1r0U76SdmbIJerP3pqN5frqw3MviAqVPoJhyvsSpjCT5EVW8DFDTQ+Q0wIcnc1ZzbVBh+IjhHsC0QnnVX8/tHrH9TcwYPCN9+OVop0DAvBkPb/+lZmaM/xObPDSv63GR+DltRQb1UbmJ6Y39XvEbuuTfvYSLvOVTNw2JJjav1Yz6cew4trb7Q2v4OuGRC3okulc+6NvFwNXjgDTf3JrEcsK1A/rMn1joUHYfgo6BpiS8oZYlYyvy+ZTipHCo051YQXFTYd9G/stsxji85 erhan@DESKTOP-LH1I4HV\nnurik:ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDFnnFiRt9UQDwR0I001ss3Zo9eAjLj/j85JA822IChRhGkPBZP8TT4w1NyKv6s/KQrkgrsN6zSg+pXXP99Lp6tsxEr5f9AZRoraacpNWsX8rS1PilW2SjJAa3az85tlt7G8V7aDJ0fMI8qHWJUeCSv2T/FwS5bQ4YlPEWKcyHy4h50jNygrm3574T3uZtJ68YYxT6IAXyc4Tpik1jP6NU1TrJsfiU7qLi7ph0fd+dgaOOa+QaEoT2fLo9PiCyuFEu2blMPx9Mv5DWls40wcEGzsbA15iHoUIaEqU7zKlxRTU1FTbeQjk0QzyMWQCNIVVL+FLKq5f+A0U2ktcQ+5GSR"
  }
  tags = ["http-server", "https-server"]
}
output "instance_ip_address" {
  value = google_compute_instance.example_instance.network_interface[0].access_config[0].nat_ip
  description = "The external IP address of the VM"
}