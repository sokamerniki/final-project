from django.contrib import admin
from .models import Analytics, Fruit, Order
# Register your models here.
admin.site.register(Fruit)
admin.site.register(Order)
admin.site.register(Analytics)